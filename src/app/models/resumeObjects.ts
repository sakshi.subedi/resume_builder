/*
 * pre-commit-angular-base
 * Copyright (C) 2019 iSchoolConnect
 * mailto:support AT ischoolconnect DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
interface Resume {
    personalDetails: PersonalDetails;
    educations: Education[];
    internships;
    certifications: string[];
    projects: Project[];
}

interface PersonalDetails {
    firstName: string;
    lastName: string;
    email: string;
    mobileNumber: string;
    homeNumber?: string;
    dateOfBirth;
    languages: string[];
    hobbies: string[];
    address: string;
}

interface Education {
    instituteName: string;
    startYear: number;
    endYear: number;
    courseName: string;
    description: string;
}

interface Internship {
    orgName: string;
    startYear: number;
    endYear: number;
    roleOrDesignation: string;
    description: string;
}

interface Project {
    projectTitle: string;
    projectDescription: string;
}

interface ResumeFormToSheets {
    full_name: string;
    email: string;
    mobileNumber: number;
    homeNumber?: number;
    dateOfBirth: string;
    languages: string;
    hobbies: string;
    address: string;
    // Education
    institute_one: string;
    start_year_one: number;
    end_year_one: number;
    course_one_name: string;
    description_one?: string;

    institute_two: string;
    start_year_two: number;
    end_year_two: number;
    course_two_name: string;
    description_two?: string;

    institute_three: string;
    start_year_three: number;
    end_year_three: number;
    course_three_name: string;
    description_three?: string;
    // Internship
    org_one_name: string;
    start_date_one: string;
    end_date_one: string;
    role_designation_one: string;
    intern_description_one: string;

    org_two_name: string;
    start_date_two: string;
    end_date_two: string;
    role_designation_two: string;
    intern_description_two: string;

    org_three_name: string;
    start_date_three: string;
    end_date_three: string;
    role_designation_three: string;
    intern_description_three: string;

    // Project
    project_one_title: string;
    project_one_description: string;

    project_two_title: string;
    project_two_description: string;

    project_three_title: string;
    project_three_description: string;
    // Extracurricular
}

export { Resume, PersonalDetails, Education, Internship, Project };
