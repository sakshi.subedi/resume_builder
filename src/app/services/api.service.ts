/*
 * pre-commit-angular-base
 * Copyright (C) 2019 iSchoolConnect
 * mailto:support AT ischoolconnect DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
@Injectable({
    providedIn: "root",
})
export class ApiService {
    private SHEET_URL = environment.sheet_url;

    constructor(private _http: HttpClient) {
        // return;
    }

    sendDetailsToSheet = (details: object) => {
        // console.log("details in apiService: ", details);

        const formData = new FormData();
        Object.keys(details).forEach((key) => {
            formData.append(key, details[key]);
        });

        return this._http.post(this.SHEET_URL, formData);
        // return this._http.get(this.SHEET_URL);
    };
}
