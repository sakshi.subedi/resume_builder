/*
 * pre-commit-angular-base
 * Copyright (C) 2019 iSchoolConnect
 * mailto:support AT ischoolconnect DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
import { Component } from "@angular/core";
import { MatDialog } from "@angular/material";
import { GetResumeDialogComponent } from "src/app/components/get-resume-dialog/get-resume-dialog.component";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
})
export class AppComponent {
    title = "campus-resume-tool";

    constructor(public dialog: MatDialog) {
        //
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(GetResumeDialogComponent, {
            width: "300px",
            data: { email: "" },
        });

        dialogRef.afterClosed().subscribe((result): void => {
            // console.log("The dialog was closed with data as :", result);
        });
    }
}
